var neuralNetwork = neuralNetwork || {};

(function(global) {
  "use strict";
    // Clock -----------------------------------------------------------------
    function Clock(){
        this.currentTime = 0;
        this.stepSize = 1; //1 millisecond
    }

    Clock.prototype.time = function(){
        return this.currentTime;
    }
    Clock.prototype.getStepSize = function(){
        return this.stepSize();
    }
    Clock.prototype.reset = function(){
        this.currentTime = 0;
    }
    Clock.prototype.inc = function(steps){
        this.currentTime = this.currentTime + this.stepSize * steps;
        return this.currentTime;
    }

    function NeuralNetworkInitializer(neuralNet)
    {
        this.neuralNet = neuralNet;
        this.loader = new THREE.OBJLoader();
        this.verticesFile = './static/models/brain_vertex_low.obj';
        this.connectivityFile = "./static/models/connectivity.csv";
    };

    NeuralNetworkInitializer.prototype.initialize = function()
    {
        this.loadConnectivity();
    };

    NeuralNetworkInitializer.prototype.buildConnectivity = function(data)
    {
        var connectivityMatrix = this.neuralNet.connectivityMatrix;
        for(var q = 0;q<188;q++)
            connectivityMatrix[q] = data[q];
        this.loadVertices();
    };

    NeuralNetworkInitializer.prototype.loadSettings = function(){};

    NeuralNetworkInitializer.prototype.logger = function()
    {
        return new logger.Logger(this.loggerFileClass());
    }; 

    NeuralNetworkInitializer.prototype.verticesCallback = function(loadedObject)
    {
        this.neuralNet.constructNeuralNetwork(loadedObject);
    };

    function NodeNNInitializer(neuralNet)
    {
        NeuralNetworkInitializer.call(this, neuralNet);
    };

    NodeNNInitializer.prototype = Object.create(NeuralNetworkInitializer.prototype);

    NodeNNInitializer.prototype.loggerFileClass = function()
    {
        return logger.NodeFiles;
    };

    NodeNNInitializer.prototype.loadVertices = function()
    {
        var self = this;
        fs.readFile(this.verticesFile, 'utf8', function (err, data) {
                var parsedData = self.loader.parse(data)
                self.verticesCallback(parsedData);
            });
    };

    NodeNNInitializer.prototype.loadConnectivity = function(){
        var self = this;
        fs.readFile(this.connectivityFile, 'utf8', function (err, data) {
            var matrix = d3.csv.parse(data);
            self.buildConnectivity(matrix);
        });
    };

    NodeNNInitializer.prototype.loadSettings = function(){
        var data = fs.readFileSync("./static/models/settings.json");
        all_settings = JSON.parse(data);
        inject_behavior_to_settings(all_settings);
    };

    function BrowserNNInitializer(neuralNet)
    {
        NeuralNetworkInitializer.call(this, neuralNet);
    };

    BrowserNNInitializer.prototype = Object.create(NeuralNetworkInitializer.prototype);

    BrowserNNInitializer.prototype.loadVertices = function()
    {
        var self = this;
        this.loader.load(this.verticesFile, function(objects){
            self.verticesCallback(objects);
        });
    }; 

    BrowserNNInitializer.prototype.verticesCallback = function(loadedObject)
    {
        NeuralNetworkInitializer.prototype.verticesCallback.call(this, loadedObject);
        this.neuralNet.buildVisualization(loadedObject);
    };

    BrowserNNInitializer.prototype.loadConnectivity = function(){
        var self = this;
        d3.csv(this.connectivityFile, function (matrix) {
            self.buildConnectivity(matrix);
        });
    };

    BrowserNNInitializer.prototype.loggerFileClass = function()
    {
        return all_settings.network_settings.shouldLog ? logger.AjaxFiles : logger.NullFiles;
    };         

    // Neural Network --------------------------------------------------------
    function NeuralNetwork(scene) {
        this.initialized = false;
        this.scene = scene || null;
        this.browser = scene != null;
        var initializerClass = this.browser ? BrowserNNInitializer : NodeNNInitializer;
        this.initializer = new initializerClass(this);
        this.logger = this.initializer.logger();
        this.initializer.loadSettings(); 
        this.numberExcite = 0;
        this.numberInhibit = 0;

        // settings
        this.verticesSkipStep = 1; //2
        this.maxAxonDist = all_settings.network_settings.AxonDistance; //default 8
        //this.maxAxonDist = all_settings.network_settings.AxonDistanceInhibitor; //default 4
        this.maxConnectionPerNeuron = all_settings.network_settings.NeuronConnection; //default 6
        //this.maxConnectionPerNeuronInhibitor = all_settings.network_settings.NeuronConnectionInhibitor; //default 20

        this.firing_threshold = all_settings.network_settings.firing_threshold; // threshold to fire signal (not used yet)

        this.currentMaxSignals = 8000;
        this.limitSignals = 200000;

        this.signalMinSpeed = 0.035;
        this.signalMaxSpeed = 0.065;

        // NN component containers
        this.allNeurons = [];
        this.allSignals = [];
        this.allAxons = [];

        
        // neuron
        this.neuronSize = 0.7;
        this.neuronOpacity = 1.0;
        if (scene)
        {
            this.particlePool = new particlePool.ParticlePool(this.limitSignals); // *************** ParticlePool must bigger than limit // axon
            this.excitorAxonDescription = new descriptions.AxonsDescription(0x0099ff);
            this.inhibitorAxonDescription = new descriptions.AxonsDescription(0xff5757);
            this.excitorNeuronDescription = new descriptions.NeuronsDescription(this, 0x00ffff);
            this.inhibitorNeuronDescription = new descriptions.NeuronsDescription(this, 0xff0037);
        }



        // info api
        this.numNeurons = 0;
        this.numAxons = 0;
        this.numSignals = 0;
        // probably shouldn't be hardcoded
        this.numActiveAstrocytes = 7241;
        this.regenSign = 1;
        this.lastRegenUpdate = 0;
        this.lastNeuronDecay = 0;
        //connectivity matrix between different regions
        this.connectivityMatrix =[];

        // initialize NN
        this.initNeuralNetwork();


    }
    NeuralNetwork.prototype.updateRegeneration = function(currentTime) {
    all_settings.astrocyte_settings.updateRegeneration(currentTime, this.logger);
    }


    NeuralNetwork.prototype.constructNeuralNetwork = function(loadedObject){
        var loadedMesh = loadedObject.children[0];
        var vertices = loadedMesh.geometry.vertices;

        this.initNeurons(vertices);
        this.connectNeurons();
        this.initialized = true;
        console.log('Neural Network initialized');
    };

    NeuralNetwork.prototype.buildVisualization = function(loadedObject)
    {
        var loadedMesh = loadedObject.children[0];

        // render loadedMesh
        loadedMesh.material = new THREE.MeshBasicMaterial({
            transparent: true,
            opacity: 0.05,
            depthTest: false,
            color: 0x0088ff,
            blending: THREE.AdditiveBlending
        });
        this.scene.add(loadedObject);
        this.createNeuronVisualization();
        this.createAxonsVisualization();
        this.createParticleVisualization();
        document.getElementById('loading').style.display = 'none'; // hide loading animation when finish loading model
        //this.regenerationFunction();
        //this.decayFunction();
    }; 

    NeuralNetwork.prototype.initNeuralNetwork = function() {
        this.logger.createLogs();
        this.logger.logSettings(all_settings);
        this.initializer.initialize();
    }

    NeuralNetwork.prototype.printRegions = function(){
        for(var i = 0; i<this.allNeurons.length; i++){
            var n = this.allNeurons[i];
            console.log("neuron # "+i+" region: "+n.region+"position: "+n.xPos+" "+n.yPos+" "+n.zPos);
        }
    };

    NeuralNetwork.prototype.randomPosition = function(pos)
    {
        var answer = {}
        var keys = ["x", "y", "z"];
        for (var i = 0; i < 3; i++)
        {
            var k = keys[i];
            answer[k] = (Math.random()*18)-9 + pos[k];
        }
        return answer;
    };

    NeuralNetwork.prototype.initNeurons = function(inputVertices) {
        console.log("init Neurons");
        var numInhibitors = 0;
        var idx = 0;
        for (var i = 0; i < inputVertices.length; i += this.verticesSkipStep) {
            for(var q = 0; q<40; q++){
                var pos = this.randomPosition(inputVertices[i]);
                var excitor = true;
                //every 5th neuron is inhibitory
                if (q % (5*this.verticesSkipStep) == 0) {
                    excitor = false;
                    numInhibitors++;
                }
                if (all_settings.network_settings.izhikevich != 0){
                    var n = new model.IzhikevichNeuron(all_settings, pos.x, pos.y, pos.z, excitor, idx);
                } else {
                    var n = new model.Neuron(all_settings, pos.x, pos.y, pos.z, excitor, idx);
                }
                idx = idx + 1;
                n.region = i+1;

                this.allNeurons.push(n);
            }
        }

        this.numberExcite = this.allNeurons.length - numInhibitors;
        this.numberInhibit = numInhibitors;
 
        for(var i = 0; i < this.allNeurons.length; i++){
            n = this.allNeurons[i];
            this.logger.logRegion(i+1, n.region);
        }
        this.logger.flushAll();
        //this.printRegions();

    };

    NeuralNetwork.prototype.connectNeurons = function () { 
        self = this;
        var allNeuronsLength = this.allNeurons.length;
        for (var j=0; j<allNeuronsLength; j++) {
            var n1 = this.allNeurons[j];
            this.allNeurons.slice(j+1).forEach(function(n2){
                // connect neuron if distance ... and limit connection per neuron to not more than x
                n1.tryConnect(n2, this);
            }, this);
        }
        this.logger.flushAll();
    };

    NeuralNetwork.prototype.populateAxonDescriptions = function()
    {
        this.allAxons.forEach(function(axon){
            if( axon.type === model.INHIBITOR)
                this.inhibitorAxonDescription.constructArrayBuffer(axon.axonVisualization);
            else
                this.excitorAxonDescription.constructArrayBuffer(axon.axonVisualization);
        }, this);
    }

 NeuralNetwork.prototype.populateNeuronDescriptions = function()
    {
        this.allNeurons.forEach(function(neuron){
            if( neuron.type === model.INHIBITOR)
                this.inhibitorNeuronDescription.push(neuron);
            else
                this.excitorNeuronDescription.push(neuron);
        }, this);
    }

    NeuralNetwork.prototype.createAxonsVisualization = function () {
        this.populateAxonDescriptions();
        this.excitorAxonMesh = this.excitorAxonDescription.mesh();
        this.inhibitorAxonMesh = this.inhibitorAxonDescription.mesh();
        this.scene.add(this.excitorAxonMesh);
        this.scene.add(this.inhibitorAxonMesh);
    };

    NeuralNetwork.prototype.createNeuronVisualization = function ()
    {
        this.populateNeuronDescriptions();
        this.excitorParticles = this.excitorNeuronDescription.mesh();
        this.inhibitorParticles = this.inhibitorNeuronDescription.mesh();
        this.scene.add(this.excitorParticles);
        this.scene.add(this.inhibitorParticles);
    };

    NeuralNetwork.prototype.createParticleVisualization = function ()
    {
        this.particlePool.buildVisualization(this.scene);
    };



    NeuralNetwork.prototype.update = function(currentTime) {
        if (currentTime == 100000){
                this.logger.flushAll()

        }
        if (!this.initialized) return;
        this.updateRegeneration(currentTime);

        if (this.lastNeuronDecay + all_settings.network_settings.decayTime < currentTime)
        {
            this.allNeurons.forEach(function(n){n.decay();});
            this.lastNeuronDecay = currentTime
        }

        var n, ii;

        // update neurons state and release signal
        for (ii = 0; ii < this.allNeurons.length; ii++) {
            n = this.allNeurons[ii];
            // the astrocyte we're taking energy from
            var result = n.step(this, currentTime);
                if (result[0] === true)
                    this.logger.logFiring(currentTime, ii+1, result[1]);
                else if (result[1] != null)
                    this.logger.logMissEnergy(currentTime, ii+1, result[1]);
        }
        for (ii = 0; ii < this.allNeurons.length; ii++) {
            n = this.allNeurons[ii];
            n.replenishAstrocyte(currentTime);
        }       

        self = this;
        this.allSignals.map(function(signal){
            signal.travel(currentTime, self.logger);
            if (!signal.alive)
                signal.freeParticle();
        });
        this.allSignals = this.allSignals.filter(function(signal)
        {
            return signal.alive;
        });


        if (this.browser)// update particle pool vertices
            this.particlePool.update();

        // update info for GUI
        this.updateInfo(currentTime);

    };

    NeuralNetwork.prototype.releaseSignalAt = function(neuron) {
        var signals = neuron.createSignal(this.particlePool, this.signalMinSpeed, this.signalMaxSpeed);
        for (var ii = 0; ii < signals.length; ii++) {
            var s = signals[ii];
            this.allSignals.push(s);
        }
    };

    NeuralNetwork.prototype.updateInfo = function(currentTime) {
        this.numNeurons = this.allNeurons.length;
        this.numAxons = this.allAxons.length;
        this.numSignals = this.allSignals.length;
        var activeAstros = 0;
        var totalEnergy = 0;
        for (var i = 0; i < this.numNeurons; i++) {
            var astrocyte = this.allNeurons[i].astrocyte;
            totalEnergy += astrocyte.availableEnergy;
            if (astrocyte.hasEnergy())
                activeAstros++;
        }
        this.numActiveAstrocytes = activeAstros;

        this.logger.logEnergy(currentTime, activeAstros, totalEnergy);

    };

    NeuralNetwork.prototype.updateSettings = function() {
        this.inhibitorNeuronDescription.updateSettings();
        this.excitorNeuronDescription.updateSettings()

        this.excitorAxonDescription.updateSettings();
        this.inhibitorAxonDescription.updateSettings();

        this.particlePool.updateSettings();
    };

    var all_settings = {
        astrocyte_settings : {
            minEnergy: 0.0, // default min
            maxEnergy: 1.0, // default max
            fireEnergy: 0.125, // the amount that depletes on firing
            replenishEnergy: 0.08, // amount of energy astrocyte regenerates
            regenerationTime: 150, // time needed for energy to regenerate in milliseconds
            //minThreshold: 0.125, // energy level at which the astrocyte starts regenerating energy
            minThreshold: 0.02, //
            maxThreshold: 0.08, //
            frequency: 100, // in milliseconds
            amplitude: 0.01, // increased by this amount
            regenSign: 1,
            lastRegenUpdate : 0
        },
        network_settings : {
            izhikevich: true, //anything but 0 for izhikevich neuron model
            firing_threshold: 0.60, // neuron fires when reaching this amount.
            signal_weight: 0.40, // energy of neuron increases by this amount per signal.
            AxonDistance: 10, //default
            //AxonDistanceInhibitor: 4, //default
            NeuronConnection: 6, //default
            //NeuronConnectionInhibitor: 20, //default
            decayTime: 50, //time needed for neurons to decay,
            shouldLog: false,

    }};

    function inject_behavior_to_settings(settings)
    {
        settings.network_settings['reload'] = function() {
                window.clock = new Clock();
                window.scene = new THREE.Scene();
                var nn = new NeuralNetwork(window.scene, window.clock);
                window.neuralNet = nn;
                var to_delete = window.to_delete;
                window.to_delete = window.init_gui(window.gui, nn, all_settings, to_delete);
            };
        settings.network_settings['topView'] = function (cameraCtrl){
                cameraCtrl.reset();
                cameraCtrl.object.position.set(0,0,200);

            };

        settings.network_settings['rightView'] = function (cameraCtrl){
                cameraCtrl.reset();
                cameraCtrl.object.position.set(200,0,0);
                cameraCtrl.object.up.set(1, 0, 1)

            }; 
            settings.network_settings['leftView'] = function (cameraCtrl){
                cameraCtrl.reset();
                cameraCtrl.object.position.set(-200,0,0);
                cameraCtrl.object.up.set(1, 0, 1)

            };          
        var self = settings.astrocyte_settings;
        settings.astrocyte_settings["updateRegeneration"] = function(currentTime, logger) {
            var timeSinceLastUpdate = currentTime - self.lastRegenUpdate;
            if(timeSinceLastUpdate >= self.frequency){
                var replenish = (self.replenishEnergy += self.regenSign*self.amplitude);
                self.lastRegenUpdate = currentTime;
                var up = replenish > self.maxThreshold;
                var down = replenish < self.minThreshold;
                if (up || down)
                {
                    self.regenSign *= -1;
                    if (up)
                      self.replenishEnergy = self.maxThreshold;
                    else
                      self.replenishEnergy = self.minThreshold;
                }
            }};
    };
    inject_behavior_to_settings(all_settings);



  global.Clock = Clock;
  global.NeuralNetwork = NeuralNetwork;
  global.all_settings = all_settings;
})(neuralNetwork);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.neuralNetwork = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(neuralNetwork);
