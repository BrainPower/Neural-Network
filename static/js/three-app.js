
(function main() {
    "use strict";
    
    // Main ------------------------------------------------------------------

    if (!Detector.webgl) {
        Detector.addGetWebGLMessage();
        document.getElementById('loading').style.display = 'none'; // hide loading animation when finish loading model
    }

    var container, stats;
    var scene, camera, cameraCtrl, renderer;

    // ---- scene
    container = document.getElementById('canvas-container');
    scene = window.scene = new THREE.Scene();

    // ---- camera
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 2000);
    // camera orbit control
    cameraCtrl = new THREE.TrackballControls(camera, container);
    var cameraClock = new THREE.Clock();
    cameraCtrl.object.position.set(0,0,200);

    // ---- renderer
    renderer = new THREE.WebGLRenderer({
        antialias: true,
        alpha: true
    });
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    // ---- stats
    stats = new Stats();
    container.appendChild(stats.domElement);

    // ---- scene settings
    var scene_settings = {
        pause: false,
        bgColor: 0x0d0d0f
    };

    // Neural Net
    var clock = window.clock = new neuralNetwork.Clock();
    var neuralNet = window.neuralNet = new neuralNetwork.NeuralNetwork(scene);
    var all_settings = neuralNetwork.all_settings;


    // ---------- GUI ----------

    function init_gui(gui, neuralNet, all_settings, to_delete)
    {
        to_delete.forEach(function(element){
            gui.removeFolder(element);
        })
        to_delete = [];
        var gui_info = gui.addFolder('Network information');
        to_delete.push(gui_info.name);
        gui_info.add(neuralNet, 'numNeurons').name('Neurons');
        gui_info.add(neuralNet, 'numNeurons').name('Energy Pools');
        gui_info.add(neuralNet, 'numAxons').name('Axons (connections)');
        gui_info.add(neuralNet, 'numSignals', 0, neuralNet.numAxons).name('Current action potentials');
        gui_info.add(neuralNet, 'numActiveAstrocytes', 0, neuralNet.numActiveAstrocytes).name('Viable energy pools');
        gui_info.add(all_settings.astrocyte_settings, 'minEnergy').name('Min in energy pool');
        gui_info.add(all_settings.astrocyte_settings, 'maxEnergy').name('Max in energy pool');
        gui_info.add(all_settings.astrocyte_settings, 'fireEnergy', 0, 1).name('Min energy for firing');
        gui_info.autoListen = false;


        var gui_settings = gui.addFolder('Network settings');
        to_delete.push(gui_settings.name);
        gui_settings.add(neuralNet, 'currentMaxSignals', 0, neuralNet.limitSignals).name('Max signals');
        gui_settings.add(all_settings.network_settings, 'AxonDistance', 0, 20).name('Max axon distance');
        //gui_settings.add(all_settings.network_settings, 'AxonDistanceInhibitor', 0, 20).name('Max Axon Distance Inhibitor');
        gui_settings.add(all_settings.network_settings, 'NeuronConnection', 0, 20).name('Max neuron connections');
        //gui_settings.add(all_settings.network_settings, 'NeuronConnectionInhibitor', 0, 20).name('Max Inhibitor Neuron Connections');
        gui_settings.add(neuralNet, 'signalMinSpeed', 0.01, 0.1, 0.01).name('Signal min speed');
        gui_settings.add(neuralNet, 'signalMaxSpeed', 0.01, 0.1, 0.01).name('Signal max speed');
        gui_settings.add(all_settings.network_settings, 'shouldLog').name('Logger');
        gui_settings.add(all_settings.network_settings, 'izhikevich').name("Izhikevich Neuron Model");
        gui_settings.add(all_settings.network_settings, 'reload');


        var gui_settings = gui.addFolder('Energy pool settings');
        to_delete.push(gui_settings.name);
        //gui_settings.add(all_settings.astrocyte_settings, 'minThreshold', 0, 1).name('Threshold for energy regeneration');
        gui_settings.add(all_settings.astrocyte_settings, 'replenishEnergy', 0, 1).name('Replenish rate').listen();
        gui_settings.add(all_settings.astrocyte_settings, 'regenerationTime', 0, 500).name('Regen. time (ms)');
        gui_settings.add(all_settings.astrocyte_settings, 'minThreshold', 0, 1).name('Minimum threshold');
        gui_settings.add(all_settings.astrocyte_settings, 'maxThreshold', 0, 1).name('Maximum threshold');
        gui_settings.add(all_settings.astrocyte_settings, 'frequency', 0, 1000).name('Freq. of rate change');
        gui_settings.add(all_settings.astrocyte_settings, 'amplitude', 0, 1).name('Amp. of rate change');

        // controller.onFinishChange(function(value){
        //  //clearInterval(functionRegeneration);
        //  window.neuralNet.regenerationFunction();
        // });

        var gui_settings = gui.addFolder('Activation function settings');
        to_delete.push(gui_settings.name);
        gui_settings.add(all_settings.network_settings, 'firing_threshold', 0, 1).name("Membrane pot. thresh.");
        gui_settings.add(all_settings.network_settings, 'signal_weight', 0, 1).name("Signal weight");
        gui_settings.add(all_settings.network_settings, 'decayTime', 0, 1000).name("Decay time in ms");

        var gui_settings = gui.addFolder('Visual settings');
        to_delete.push(gui_settings.name);
        gui_settings.add(neuralNet.particlePool, 'pSize', 0.2, 2).name('Signal size');

        var excitorNeuron = neuralNet.excitorNeuronDescription;
        var inhibitorNeuron = neuralNet.inhibitorNeuronDescription;
        gui_settings.add(neuralNet, 'neuronSize', 0, 2).name('Neuron size');
        gui_settings.add(neuralNet, 'neuronOpacity', 0, 1.0).name('Neuron opacity');
        gui_settings.addColor(excitorNeuron, 'color').name('Excitatory neuron color');
        gui_settings.addColor(inhibitorNeuron, 'color').name('Inhibitory Color');

        var excitorAxon = neuralNet.excitorAxonDescription;
        var inhibitorAxon = neuralNet.inhibitorAxonDescription;

        gui_settings.add(excitorAxon, 'opacityMultiplier', 0.0, 5.0).name('Excitatory axon opacity mult');
        gui_settings.add(inhibitorAxon, 'opacityMultiplier', 0.0, 5.0).name('Inhibitory axon opacity mult');
        gui_settings.addColor(neuralNet.particlePool, 'pColor').name('Signal color');

        gui_settings.addColor(excitorAxon, 'color').name('Excitatory axon color');
        gui_settings.addColor(inhibitorAxon, 'color').name('Inhibitory axon color');

        gui_settings.addColor(scene_settings, 'bgColor').name('Background');

        gui.add({topView : all_settings.network_settings.topView.bind(this, cameraCtrl)}, 'topView').name('Top View');
        gui.add({rightView : all_settings.network_settings.rightView.bind(this, cameraCtrl)}, 'rightView').name('Right View');;
        gui.add({leftView : all_settings.network_settings.leftView.bind(this, cameraCtrl)}, 'leftView').name('Left View');;

        function updateNeuralNetworkSettings() {
            neuralNet.updateSettings();
        }

        for (var i in gui_settings.__controllers) {
            gui_settings.__controllers[i].onChange(updateNeuralNetworkSettings);
        }

        window.gui_info = gui_info;

        return to_delete;
    }

    dat.GUI.prototype.removeFolder = function(name) {
        var folder = this.__folders[name];
        if (!folder) {
            return;
        }
        folder.close();
        this.__ul.removeChild(folder.domElement.parentNode);
        delete this.__folders[name];
        this.onResize();
    }

    var gui = new dat.GUI();
    gui.width = 425;
    window.gui = gui;
    window.init_gui = init_gui;
    window.to_delete = init_gui(gui, neuralNet, all_settings, [])

    //gui_info.open();
    // a_settings.open();

    function updateGuiInfo() {
        var gui_info = window.gui_info;
        for (var i in gui_info.__controllers) {
            gui_info.__controllers[i].updateDisplay();
        }
    }

    // ---------- end GUI ----------

    (function run() {

        neuralNet = window.neuralNet;
        scene = window.scene;
        requestAnimationFrame(run);
        renderer.setClearColor(scene_settings.bgColor, 1); // change to 0 for white background (looks bad)

        if (!scene_settings.pause) {
            if (clock.time() >= 100000){
                scene_settings.pause = true;
            } else {
                clock.inc(1);
                neuralNet.update(clock.time());
                updateGuiInfo();
            }
        }
        var delta = cameraClock.getDelta();
        cameraCtrl.update(delta);

        renderer.render(scene, camera);
        stats.update();

    })();


    window.addEventListener('keypress', function(event) {
        if (event.keyCode === 32) { // if spacebar is pressed
            event.preventDefault();
            scene_settings.pause = !scene_settings.pause;
        }
    });


    window.addEventListener('resize', function onWindowResize() {
        var w = window.innerWidth;
        var h = window.innerHeight;
        camera.aspect = w / h;
        camera.updateProjectionMatrix();
        renderer.setSize(w, h);
    }, false);

}());
