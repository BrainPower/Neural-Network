"use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    var self = window.self; // in ordinary browser self is windows.self.
  } else {
    var self = global; // in nodejs
  }

var d3 = require("d3");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var fs = require("fs");
var mkdirp = require("mkdirp");