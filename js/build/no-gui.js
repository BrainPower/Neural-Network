var logger = logger || {};

(function(global) {
  "use strict";
    function LogType(logger, url, directory, softlimit)
    {
        this.logger = logger;
        this.directory = directory;
        this.url = url;
        this.store = [];
        this.softlimit = softlimit || 1000;
    };

    LogType.prototype.log = function(objects) {
        this.store.push(objects.join(",") + "\n");
        if(this.store.length >= this.softlimit){
            this.flush();
        }
    };

    LogType.prototype.flush = function(){
        if(this.store.length > 0){
            this.logger.sendToServer(this.store, this.url);
            this.store = [];
        }
    };

    // Logger ----------------------------------------------------------------
    function Logger(fileclass){
        this.fileHandle = new fileclass(this);
        this.firing = new LogType(this, "firing", "Firing");
        this.input = new LogType(this, "input", "Input");
        this.potential  = new LogType(this, "potential", "Potential");
        this.missed = new LogType(this, "miss", "MissEnergy");
        this.replenish  = new LogType(this, "replenish", "ReplenishEnergy", 20);
        this.connections  = new LogType(this, "connection", "Connections");
        this.weights = new LogType(this, "conweights", "ConWeights");
        this.regions = new LogType(this, "region", "Region", 200);
        this.energy = new LogType(this, "energy", "Energy");
        this.settings = new LogType(this, "settings", "Settings", 1);
        this.all = [this.firing, this.input, this.potential, this.missed,
                    this.replenish, this.connections, this.weights, 
                    this.regions, this.energy, this.settings];
    }

    Logger.prototype.logFiring = function(time, neuron, potential){
        this.firing.log([time, neuron, 1]);
        this.potential.log([time, neuron, potential]);
    }
    Logger.prototype.logInput = function(time, from, to, type){
        this.input.log([time, from.idx, to.idx, type]);
    }
    Logger.prototype.logMissEnergy = function(time, neuron, energy){
        this.missed.log([time, neuron, energy]);
    }
    Logger.prototype.logRep = function(time, energy){
        this.replenish.log([time, energy]);
    }

    Logger.prototype.logEnergy = function(time, astrocytes, totalEnergy){
        this.energy.log([time, astrocytes, totalEnergy]);
    }
    Logger.prototype.logCon = function(n1, n2, weight){
        this.connections.log([n1, n2, 1]);
        this.weights.log([n1, n2, weight])
    }
    Logger.prototype.logRegion = function(neuron, region){
        this.regions.log([neuron, region])
    }

    Logger.prototype.logSettings = function(settings)
    {
        this.settings.log([JSON.stringify(settings),]);
    }

    Logger.prototype.flushAll = function(){
        for (var i = 0; i < this.all.length; i++)
            this.all[i].flush();
    }
    Logger.prototype.sendToServer = function(entries, url){
        this.fileHandle.sendToServer(entries, url);
    }
    Logger.prototype.createLogs = function(){
        this.fileHandle.createLogs();
    }


    function AjaxFiles(logger)
    {};


    AjaxFiles.prototype.sendToServer = function(entries, url){
        $.ajax({
            type: "POST",
            url: "/"+url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(entries),
            dataType: 'json',
            success: function(data) {
//              console.log("success");
            },
            error: function(error) {
              console.log(error);
            }
        });
    }
    AjaxFiles.prototype.createLogs = function(){
        $.ajax({
            type: "POST",
            url: "/createLogs",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify("Create the logs"),
            dataType: 'json',
            success: function(data) {
//              console.log("success");
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function NullFiles(logger)
    {};

    NullFiles.prototype.sendToServer = function(entries, url){}
    NullFiles.prototype.createLogs = function(){}

    function NodeFiles(logger)
    {
        this.logger = logger;
        this.files = {};
    };

    NodeFiles.prototype.sendToServer = function(entries, url){
        var fd = this.files[url];
        fs.write(fd, entries.join(''));
    };
    NodeFiles.prototype.twoDigits = function(n)
    {
        return ("0" + n).slice(-2);
    };

    NodeFiles.prototype.buildTimestamp = function()
    {
        var date = new Date();
        var year = date.getFullYear().toString();
        var month = (date.getMonth() + 1).toString();
        var day = date.getDate().toString();
        var hours = date.getHours().toString();
        var minutes = date.getMinutes().toString();
        var seconds = date.getSeconds().toString();
        var first = [year, this.twoDigits(month), this.twoDigits(day)].join('-');
        var second = [hours, minutes, seconds].map(this.twoDigits).join('-');
        this.timestamp = first + '_' + second;        
    }
    NodeFiles.prototype.createLogs = function(){
        this.buildTimestamp();
        var baseDirectory = '../BrainPowerLogs';
        var self = this;
        this.logger.all.forEach(function(logType){
            var directory = baseDirectory + '/' + logType.directory;
            mkdirp.sync(directory);
            self.files[logType.url] = fs.openSync(directory + '/' + self.timestamp, 'w');
        });

    };    

    global.Logger = Logger;
    global.NullFiles = NullFiles;
    global.AjaxFiles = AjaxFiles;
    global.NodeFiles = NodeFiles;
})(logger);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.logger = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(logger);


var model = model || {};

(function(global) {
  "use strict";
	const EXCITOR = 0;
	const INHIBITOR = 1;

	// Neuron ----------------------------------------------------------------

	function Neuron(all_settings, x, y, z, excitor, idx) {
        this.settings = all_settings.network_settings;

    	this.type = excitor ? EXCITOR : INHIBITOR;
    	this.connection = [];
    	// represents the 1:1 ratio astrocyte associated w/ neuron
        this.astrocyte = new model.Astrocyte(all_settings.astrocyte_settings);
        this.reset(0);
    	// the neurons this neuron is connected to
    	this.neurons = [];
    	this.lastSignalRelease = 0;
    	this.releaseDelay = 0;
    	this.prevReleaseAxon = null;
    	this.refactoryPeriod = 10;
    	this.xPos = x;
    	this.yPos = y;
    	this.zPos = z;
    	this.idx = idx + 1;

    	//neuron fires when this number passes the firing threshold
    	this.acc = -0.5;

    	this.region = -1; // which region the neuron belongs to
    	// 1 - 188, assigned during initialization

    	THREE.Vector3.call(this, x, y, z);
	}

	Neuron.prototype = Object.create(THREE.Vector3.prototype);

	//One directional connection from the neuron calling the method to the neuron in the parameter.
	Neuron.prototype.connectNeuronOneDirection = function(neuronB) {

		var neuronA = this;
		var type = neuronA.type;
		// create axon and establish connection from A to B
		var axon = new Axon(neuronA, neuronB, type);
		neuronA.connection.push(new Connection(axon, 'A'));
		neuronA.neurons.push(neuronB);
		return axon;

	};

	//Two directional connection from the neuron calling the method to the neuron in the parameter.
	//A signal can travel both ways
	Neuron.prototype.connectNeuronTwoDirection = function(neuronB) {

		var neuronA = this;
		var type = neuronA.type;
		// create axon and establish connection in both directions
		var axon = new Axon(neuronA, neuronB, type);
		neuronA.connection.push(new Connection(axon, 'A'));
		neuronB.connection.push(new Connection(axon, 'B'));
		neuronA.neurons.push(neuronB);
		neuronB.neurons.push(neuronA);
		return axon;

	};

    Neuron.prototype.resetPotentialEnergy = function() {
        this.acc = 0.125-1; //resets energy of neuron
    };

    Neuron.prototype.reset = function(currentTime) {
        this.resetPotentialEnergy();
        this.releaseDelay = 0;
        this.fired = false;
        this.receivedSignal = false;
        this.firedCount = 0;
        this.signalCount = 0;
        // reset all astrocytes
        // TODO: in the future this should be adjustable and occur constantly,
        // not just when the signal dies.
        this.astrocyte.resetEnergy(currentTime);        
    }

	Neuron.prototype.decay = function(){
        if (this.astrocyte.hasEnergy())
        {
    		var epsilon = -0.0001;
    		this.acc = 0.9 * this.acc;
    		if (this.acc > epsilon)
    			this.acc = 0;
        }
	}

    Neuron.prototype.canConnectTo = function(n2, network) {
        var r1 = parseInt(this.region);
        var r2 = parseInt(n2.region);

        if (r1 ===r2 && this.distanceTo(n2) < network.maxAxonDist)
            return true;
        else
        {
            var probFromMatrix = network.connectivityMatrix[r1-1][r2];
            probFromMatrix = Number(probFromMatrix);
            probFromMatrix = probFromMatrix / 5000;
            return Math.random() < probFromMatrix;
        }
    };
	Neuron.prototype.tryConnect = function(neuronB, network){
		var n1 = this;
		var n2 = neuronB;

        if (n1 === n2)
            return;

        if (!n1.canConnectTo(n2, network))
            return;

		var rand = Math.floor( Math.random() * 3 );

		//two directional connection
		if(rand === 0)
			var connectedAxon = n1.connectNeuronTwoDirection(n2);
	 	//one directional connection n2 to n1
		else if(rand === 1)
			var connectedAxon = n2.connectNeuronOneDirection(n1);
		//one directional connection n1 to n2
		else if(rand === 2)
			var connectedAxon = n1.connectNeuronOneDirection(n2);
		network.allAxons.push(connectedAxon);
		var rand = (Math.random()*41+80)/100;
		connectedAxon.weight = rand * (1/connectedAxon.cpLength);
		if(network.logger != null){
			network.logger.logCon(n1.idx, n2.idx, connectedAxon.weight);
			if(rand === 0)
				network.logger.logCon(n2.idx, n1.idx, connectedAxon.weight);
		}
		
	}

	Neuron.prototype.createSignal = function(particlePool, minSpeed, maxSpeed) {

		this.firedCount += 1;
		this.receivedSignal = false;

		// create signal to all connected axons
		return this.connection.filter(function(connection)
			{
				return connection.axon !== this.prevReleaseAxon;
			}, this)
			.map(function(connection)
			{
				return new Signal(connection, particlePool, minSpeed, maxSpeed);
			})
	};


	//returns active astrocyte, it's taking energy from
	Neuron.prototype.astrocyteWithEnergy = function(neighbors) {
		neighbors = typeof neighbors !== 'undefined' ? neighbors : true;
		var total = this.neurons.length;
		//console.log(" i"+this.neurons.length);
		var activeAstrocyte = null;
		// see if the astrocyte directly linked to this neuron has the energy needed to fire
		if (this.astrocyte.hasEnergy() === true) {
			return this.astrocyte;
		}

		// if we get here, the directly linked astrocyte did not have enough energy
		// check the astrocytes of surrounding neurons to see if they have enough energy
		if (neighbors){
			for (var i = 0; i < total; i++) {
				var astrocyte = this.neurons[i].astrocyteWithEnergy(false);
				if (astrocyte == null)
					return astrocyte;
			}
		}
		return null;

	};
	Neuron.prototype.willFire = function(currentTime)
	{
		var timeSinceLastFiring = currentTime - this.lastSignalRelease; // used to check if in refactory period
		if (timeSinceLastFiring >= this.refactoryPeriod && //past refactory period
			this.acc >= this.settings.firing_threshold-1) //has to be past the threshold to consider firing
				return Math.random() < 1+this.acc;
		else
			return false;
	}
	//neuron firing function with probability of firing equal to the energy level
	Neuron.prototype.fire = function() {
		this.fired = true;
		this.resetPotentialEnergy();
		// decrease energy level of astrocyte responsible for
		// giving the neuron the energy it needed to fire
		this.releaseDelay = THREE.Math.randInt(100, 1000);
	};

	Neuron.prototype.step = function(neuralNet, currentTime)
	{
		var ret = [false, null];
		if (this.willFire(currentTime))
		{
			var astrocyte = this.astrocyteWithEnergy(false);
			if (astrocyte != null) { // Astrocyte mode
				var prevacc = this.acc;
				this.fire();
				ret = [true, prevacc]
				astrocyte.deplete(currentTime);
				this.lastSignalRelease = currentTime;
				neuralNet.releaseSignalAt(this);
			} else {
				ret = [false, this.astrocyte.availableEnergy];
			}
		}
		return ret;
	}
	Neuron.prototype.replenishAstrocyte = function(currentTime){
		this.astrocyte.replenish(currentTime);
	}
	Neuron.prototype.effectiveSignal = function() {
		return (this.prevReleaseAxon.weight * this.settings.signal_weight);
	}

	//accumulation function when recieving a signal from an excitory neuron
	Neuron.prototype.buildExcitor = function() {
		this.acc = Math.min(0, this.acc + this.effectiveSignal());
	};

	//accumulation function when recieving a signal from an inhibitory neuron
	Neuron.prototype.buildInhibitor = function() {
		this.acc = Math.max(-1, this.acc - this.effectiveSignal());
	};

    function IzhikevichNeuron(all_settings, x, y, z, excitor, idx){
        Neuron.call(this, all_settings, x, y, z, excitor, idx);
        var r = Math.random();
        this.a = 0.02 + (this.type == INHIBITOR ? 0.08 * r: 0);
        this.b = this.type == INHIBITOR ? 0.25 - 0.05*r : 0.2;
        this.c = -65 + (this.type == INHIBITOR ? 0 : 15*r*r);
        this.d = this.type == INHIBITOR ? 2 : 8-6*r*r;
        this.acc = -65;
        this.u = this.b*this.acc;
    };

    IzhikevichNeuron.prototype = Object.create(Neuron.prototype);


    IzhikevichNeuron.prototype.willFire = function(currentTime)
    {
        var timeSinceLastFiring = currentTime - this.lastSignalRelease; // used to check if in refactory period
        if (timeSinceLastFiring >= this.refactoryPeriod )
            return this.acc >= 30;
    }

    IzhikevichNeuron.prototype.step = function(neuralNet, currentTime)
    {
        var answer = Neuron.prototype.step.call(this, neuralNet, currentTime);
        if ((this.acc <= 30) & (this.astrocyte.hasEnergy()))
        {
            var r = Math.random();
            var thalamic_input = this.type == INHIBITOR ? 2 * r: 5 * r;
            var v = this.acc;
            this.acc = v + 0.5 * (0.04 * v * v + 5 * v + 140 - this.u + thalamic_input);
            var v = this.acc;
            this.acc = v + 0.5 * (0.04 * v * v + 5 * v + 140-this.u + thalamic_input);
            this.u = this.u + this.a*(this.b*this.acc - this.u)
        }
        return answer;
    }

    IzhikevichNeuron.prototype.decay = function(){};

    IzhikevichNeuron.prototype.resetPotentialEnergy = function(currentTime)
    {
        this.acc = this.c;
        this.u = this.u + this.d;
    }


    //accumulation function when recieving a signal from an excitory neuron
    IzhikevichNeuron.prototype.buildExcitor = function() {
        this.acc = this.acc + this.effectiveSignal();
    };

    //accumulation function when recieving a signal from an inhibitory neuron
    IzhikevichNeuron.prototype.buildInhibitor = function() {
        this.acc = this.acc - this.effectiveSignal();
    };


	// Astrocyte -------------------------------------------------------------
	function Astrocyte(settings) {
		// replaces the if firedCount < 8
    this.settings = settings;
		this.availableEnergy = this.settings.maxEnergy;
		this.lastRefilled = 0;
	}

	// TODO: Get rid of this because we should never have a situation where the astrocyte energy is hard-reset, right?...
	// Astrocytes should just regenerate energy at a constant rate and neurons pull from it if it's there and they need it...
	Astrocyte.prototype.resetEnergy = function(currentTime) {
		//this.availableEnergy = THREE.Math.randInt(this.settings.minEnergy, this.settings.maxEnergy);
        this.availableEnergy += this.settings.replenishEnergy;
		if (this.availableEnergy > this.settings.maxEnergy)
			this.availableEnergy = this.settings.maxEnergy;
		//console.log("reset: "+ this.availableEnergy);
		this.lastRefilled = currentTime;
	};
	Astrocyte.prototype.hasEnergy = function(){
		if(this.availableEnergy >= this.settings.fireEnergy)
			return true;
		else
			return false;
	}
	//depletes energy from the astrocyte when the neuron fires
	Astrocyte.prototype.deplete = function(currentTime) {
		this.availableEnergy -= this.settings.fireEnergy; //energy needed to fire a signal
		if (this.availableEnergy <= this.settings.minEnergy) {
			this.availableEnergy = this.settings.minEnergy
		}
		this.lastRefilled = currentTime;
	};

	//regenerates energy of the astrocyte in a certain time period that can be set in the settings
	Astrocyte.prototype.replenish = function(currentTime) {
		if (currentTime - this.lastRefilled> this.settings.regenerationTime)
		{
			this.availableEnergy += this.settings.replenishEnergy;
			if(this.availableEnergy > this.settings.maxEnergy){
				this.availableEnergy = this.settings.maxEnergy;
			}
			this.lastRefilled = currentTime;
		}
	};

	// Signal ----------------------------------------------------------------

	function Signal(connection, particlePool, minSpeed, maxSpeed) {

		this.minSpeed = minSpeed;
		this.maxSpeed = maxSpeed;
		this.speed = THREE.Math.randFloat(this.minSpeed, this.maxSpeed);
		this.alive = true;
		this.t = null;
		this.startingPoint = null;
		this.axon = null;
		this.particle = particlePool ? particlePool.getParticle() : null;
		this.excitor = true;
		THREE.Vector3.call(this);
		this.setConnection(connection);

	}

	Signal.prototype = Object.create(THREE.Vector3.prototype);

	Signal.prototype.setConnection = function(Connection) {

		this.startingPoint = Connection.startingPoint;
		this.axon = Connection.axon;
		if (this.startingPoint === 'A') this.t = 0;
		else if (this.startingPoint === 'B') this.t = 1;

	};

	Signal.prototype.dispatchSignal = function(from, to, clock, logger)
	{
		this.alive = false;
		to.receivedSignal = true;
		to.signalCount++;
		to.prevReleaseAxon = this.axon;
		//checks what type of neuron sent the signal to call the correct build function
		if (from.type == EXCITOR){
			to.buildExcitor();
			if(logger != null)
				logger.logInput(clock, from, to, EXCITOR);
		}
		else if (from.type == INHIBITOR) {
			//console.log("firer = "+this.axon.neuronA.type+" reciever = "+this.axon.neuronB.type);
			//console.log("energy before = "+this.axon.neuronB.acc);
			to.buildInhibitor();
			if(logger != null)
				logger.logInput(clock, from, to, INHIBITOR);
		}

	}

	Signal.prototype.freeParticle = function(){
		if (this.particle != null)
		{
			this.particle.free();
			this.particle = null;
		}
	}

	Signal.prototype.travel = function(clock, logger) {

		var pos;
		//var temp = this.axon.getPoint(this.t);
		// console.log("direction of axon = "+this.axon.direction + "starting point = "+this.startingPoint);
		// if (this.startingPoint === 'A' && (this.axon.direction === 0 || this.axon.direction === 2)) {
		if (this.startingPoint === 'A') {
			this.t += this.speed;
			if (this.t >= 1) {
				this.t = 1;
				this.dispatchSignal(this.axon.neuronA, this.axon.neuronB, clock, logger);
			}
			//console.log("fired signal = "+this.startingPoint);

		} //else if (this.startingPoint === 'B' && (this.axon.direction === 1 || this.axon.direction === 2)) {
		else if (this.startingPoint === 'B') {
			this.t -= this.speed;
			if (this.t <= 0) {
				this.t = 0;
				this.dispatchSignal(this.axon.neuronB, this.axon.neuronA, clock, logger);
			}
			//console.log("fired signal = "+this.startingPoint);
		}

		if (this.particle != null)
		{
			pos = this.axon.getPoint(this.t);
			this.particle.set(pos.x, pos.y, pos.z);
		}

	};

  	// Axon ------------------------------------------------------------------
  	function AxonVisualization(axon)
  	{
  		this.axon = axon;
  		var neuronA = axon.neuronA;
  		var neuronB = axon.neuronB;
  		THREE.LineCurve3.call(this, neuronA, neuronB);
  		this.geom = new THREE.Geometry();
  		this.geom.vertices.push(neuronA, neuronB);
  	}
    AxonVisualization.prototype = Object.create(THREE.LineCurve3.prototype);

    function Axon(neuronA, neuronB, type) {

    	this.weight = 1;
    	this.type = type;

    	this.neuronA = neuronA;
    	this.neuronB = neuronB;
    	this.axonVisualization = new AxonVisualization(this);
    	this.cpLength = this.distance();
    }

    Axon.prototype.distance = function()
    {
    	return this.neuronA.distanceTo(this.neuronB);
    }

    Axon.prototype.getPoint = function(t)
    {
    	return this.axonVisualization.getPoint(t);
    }

    // Connection ------------------------------------------------------------
    function Connection(axon, startingPoint) {
    	this.axon = axon;
    	this.startingPoint = startingPoint;


    }


	global.Neuron = Neuron;
	global.Signal = Signal;
    global.Astrocyte = Astrocyte;
    global.EXCITOR = EXCITOR;
    global.INHIBITOR = INHIBITOR;
    global.IzhikevichNeuron = IzhikevichNeuron;

})(model);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.model = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(model);

var particlePool = particlePool || {};

(function(global) {
  "use strict";

    function ParticlePool(poolSize) {

        this.spriteTextureSignal = THREE.ImageUtils.loadTexture("./static/sprites/electric.png");

        this.poolSize = poolSize;
        this.pGeom = new THREE.Geometry();
        this.particles = this.pGeom.vertices;
        this.availableParticles = []

        this.offScreenPos = new THREE.Vector3(9999, 9999, 9999); // #CM0A r68 PointCloud default frustumCull = true(extended from Object3D), so need to set to 'false' for this to work with oppScreenPos, else particles will dissappear

        // this.pColor = 0xfff6cd;
        this.pColor = 0xff4400;
        this.pSize = 0.6;

        var particle;
        for (var ii = 0; ii < this.poolSize; ii++) {
            particle = new Particle(this);
            this.particles[ii] = particle;
            this.free(particle)
        }

    };

    ParticlePool.prototype.buildVisualization = function(scene)
    {
        // inner particle
        this.pMat = new THREE.PointCloudMaterial({
            map: this.spriteTextureSignal,
            size: this.pSize,
            color: this.pColor,
            blending: THREE.AdditiveBlending,
            depthTest: false,
            transparent: true
        });

        this.pMesh = new THREE.PointCloud(this.pGeom, this.pMat);
        this.pMesh.frustumCulled = false; // ref: #CM0A

        scene.add(this.pMesh);


        // outer particle glow
        this.pMat_outer = new THREE.PointCloudMaterial({
            map: this.spriteTextureSignal,
            size: this.pSize * 10,
            color: this.pColor,
            blending: THREE.AdditiveBlending,
            depthTest: false,
            transparent: true,
            opacity: 0.025
        });

        this.pMesh_outer = new THREE.PointCloud(this.pGeom, this.pMat_outer);
        this.pMesh_outer.frustumCulled = false; // ref:#CM0A

        scene.add(this.pMesh_outer);

    };

    ParticlePool.prototype.getParticle = function() {
        if (this.availableParticles.length > 0)
            return this.availableParticles.pop();
        return null;

    };

    ParticlePool.prototype.free = function(particle){
        particle.set(this.offScreenPos.x, this.offScreenPos.y, this.offScreenPos.z);
        this.availableParticles.push(particle);
    };

    ParticlePool.prototype.update = function() {

        this.pGeom.verticesNeedUpdate = true;

    };

    ParticlePool.prototype.updateSettings = function() {

        // inner particle
        this.pMat.color.setHex(this.pColor);
        this.pMat.size = this.pSize;
        // outer particle
        this.pMat_outer.color.setHex(this.pColor);
        this.pMat_outer.size = this.pSize * 10;

    };

    // Particle --------------------------------------------------------------
    // Private class for particle pool

    function Particle(particlePool) {

        this.particlePool = particlePool;

    }

    Particle.prototype = Object.create(THREE.Vector3.prototype);

    Particle.prototype.free = function() {
        this.particlePool.free(this);

    };

    global.ParticlePool = ParticlePool;
    global.Particle = Particle;
})(particlePool);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.particlePool = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(particlePool);

var descriptions = descriptions || {};

(function(global) {
  "use strict";

	function NeuronsDescription(neuralNet, color)
	{
		this.neuralNet = neuralNet;
		this.spriteTexture = THREE.ImageUtils.loadTexture("./static/sprites/electric.png");
		this.color = color;
		this.geom = new THREE.Geometry();
		this.material = new THREE.PointCloudMaterial({
			map: this.spriteTexture,
			blending: THREE.AdditiveBlending,
			depthTest: false,
			transparent: true,
			color: {
				type: 'c',
				value: new THREE.Color(this.color)
			}
		});
		this.updateSettings();
	}; 

	NeuronsDescription.prototype.updateSettings = function() {
		this.material.opacity = this.neuralNet.neuronOpacity;
		this.material.color.set(this.color);
		this.material.size = this.neuralNet.neuronSize;
	};
	NeuronsDescription.prototype.push = function(neuron)
	{
		this.geom.vertices.push(neuron);
	}

	NeuronsDescription.prototype.mesh = function()
	{
		return new THREE.PointCloud(this.geom, this.material);
	};

	function AxonsDescription(color)
	{
		this.opacityMultiplier = 1.0;
		this.color = color; // excitor: 0x0099ff, inhibitor: 0xff5757
		this.geom = new THREE.BufferGeometry();
		this.positions = [];
		this.indices = [];
		this.nextPositionsIndex = 0;

		this.shaderUniforms = {
			color: {
				type: 'c',
				value: new THREE.Color(this.color)
			},
			opacityMultiplier: {
				type: 'f',
				value: 1.0
			}
		};

		this.shaderAttributes = {
			opacityAttr: {
				type: 'f',
				value: []
			}
		};
	}


	AxonsDescription.prototype.constructArrayBuffer = function(axon)
	{
		var vertices = axon.geom.vertices;
		var numVerts = vertices.length;

		for (var i = 0; i < numVerts; i++) {
			this.positions.push(vertices[i].x, vertices[i].y, vertices[i].z);
			if (i < numVerts - 1) {
				var idx = this.nextPositionsIndex;
				this.indices.push(idx, idx + 1);

				var opacity = THREE.Math.randFloat(0.002, 0.2);
				this.shaderAttributes.opacityAttr.value.push(opacity, opacity);
			}
			this.nextPositionsIndex += 1;
		}		
	}

	AxonsDescription.prototype.updateSettings = function()
	{
		this.shaderUniforms.color.value.set(this.color);
		this.shaderUniforms.opacityMultiplier.value = this.opacityMultiplier;
	};

	AxonsDescription.prototype.mesh = function() 
	{
		var indices = new Uint32Array(this.indices.length);
		var positions = new Float32Array(this.positions.length);
		var opacities = new Float32Array(this.shaderAttributes.opacityAttr.value.length);

		// transfer temp-array to arrayBuffer
		transferToArrayBuffer(this.indices, indices);
		transferToArrayBuffer(this.positions, positions);
		transferToArrayBuffer(this.shaderAttributes.opacityAttr.value, opacities);
		this.indices = [];
		this.positions = [];
		this.opacities = [];

		function transferToArrayBuffer(fromArr, toArr) {
			for (var i=0; i<toArr.length; i++) {
				toArr[i] = fromArr[i];
			}
		}

		this.geom.addAttribute( 'index', new THREE.BufferAttribute(indices, 1) );
		this.geom.addAttribute( 'position', new THREE.BufferAttribute(positions, 3) );
		this.geom.addAttribute( 'opacityAttr', new THREE.BufferAttribute(opacities, 1) );


		// axons mesh
		this.shaderMaterial = new THREE.ShaderMaterial( {
			uniforms:       this.shaderUniforms,
			attributes:     this.shaderAttributes,
			vertexShader:   document.getElementById('vertexshader-axon').textContent,
			fragmentShader: document.getElementById('fragmentshader-axon').textContent,
			blending:       THREE.AdditiveBlending,
			// depthTest:      false,
			transparent:    true
		});

		this.mesh = new THREE.Line(this.geom, this.shaderMaterial, THREE.LinePieces);
		return this.mesh;
	};

	global.NeuronsDescription = NeuronsDescription;
	global.AxonsDescription = AxonsDescription;
})(descriptions);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.descriptions = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(descriptions);
var neuralNetwork = neuralNetwork || {};

(function(global) {
  "use strict";
    // Clock -----------------------------------------------------------------
    function Clock(){
        this.currentTime = 0;
        this.stepSize = 1; //1 millisecond
    }

    Clock.prototype.time = function(){
        return this.currentTime;
    }
    Clock.prototype.getStepSize = function(){
        return this.stepSize();
    }
    Clock.prototype.reset = function(){
        this.currentTime = 0;
    }
    Clock.prototype.inc = function(steps){
        this.currentTime = this.currentTime + this.stepSize * steps;
        return this.currentTime;
    }

    function NeuralNetworkInitializer(neuralNet)
    {
        this.neuralNet = neuralNet;
        this.loader = new THREE.OBJLoader();
        this.verticesFile = './static/models/brain_vertex_low.obj';
        this.connectivityFile = "./static/models/connectivity.csv";
    };

    NeuralNetworkInitializer.prototype.initialize = function()
    {
        this.loadConnectivity();
    };

    NeuralNetworkInitializer.prototype.buildConnectivity = function(data)
    {
        var connectivityMatrix = this.neuralNet.connectivityMatrix;
        for(var q = 0;q<188;q++)
            connectivityMatrix[q] = data[q];
        this.loadVertices();
    };

    NeuralNetworkInitializer.prototype.loadSettings = function(){};

    NeuralNetworkInitializer.prototype.logger = function()
    {
        return new logger.Logger(this.loggerFileClass());
    }; 

    NeuralNetworkInitializer.prototype.verticesCallback = function(loadedObject)
    {
        this.neuralNet.constructNeuralNetwork(loadedObject);
    };

    function NodeNNInitializer(neuralNet)
    {
        NeuralNetworkInitializer.call(this, neuralNet);
    };

    NodeNNInitializer.prototype = Object.create(NeuralNetworkInitializer.prototype);

    NodeNNInitializer.prototype.loggerFileClass = function()
    {
        return logger.NodeFiles;
    };

    NodeNNInitializer.prototype.loadVertices = function()
    {
        var self = this;
        fs.readFile(this.verticesFile, 'utf8', function (err, data) {
                var parsedData = self.loader.parse(data)
                self.verticesCallback(parsedData);
            });
    };

    NodeNNInitializer.prototype.loadConnectivity = function(){
        var self = this;
        fs.readFile(this.connectivityFile, 'utf8', function (err, data) {
            var matrix = d3.csv.parse(data);
            self.buildConnectivity(matrix);
        });
    };

    NodeNNInitializer.prototype.loadSettings = function(){
        var data = fs.readFileSync("./static/models/settings.json");
        all_settings = JSON.parse(data);
        inject_behavior_to_settings(all_settings);
    };

    function BrowserNNInitializer(neuralNet)
    {
        NeuralNetworkInitializer.call(this, neuralNet);
    };

    BrowserNNInitializer.prototype = Object.create(NeuralNetworkInitializer.prototype);

    BrowserNNInitializer.prototype.loadVertices = function()
    {
        var self = this;
        this.loader.load(this.verticesFile, function(objects){
            self.verticesCallback(objects);
        });
    }; 

    BrowserNNInitializer.prototype.verticesCallback = function(loadedObject)
    {
        NeuralNetworkInitializer.prototype.verticesCallback.call(this, loadedObject);
        this.neuralNet.buildVisualization(loadedObject);
    };

    BrowserNNInitializer.prototype.loadConnectivity = function(){
        var self = this;
        d3.csv(this.connectivityFile, function (matrix) {
            self.buildConnectivity(matrix);
        });
    };

    BrowserNNInitializer.prototype.loggerFileClass = function()
    {
        return all_settings.network_settings.shouldLog ? logger.AjaxFiles : logger.NullFiles;
    };         

    // Neural Network --------------------------------------------------------
    function NeuralNetwork(scene) {
        this.initialized = false;
        this.scene = scene || null;
        this.browser = scene != null;
        var initializerClass = this.browser ? BrowserNNInitializer : NodeNNInitializer;
        this.initializer = new initializerClass(this);
        this.logger = this.initializer.logger();
        this.initializer.loadSettings(); 
        this.numberExcite = 0;
        this.numberInhibit = 0;

        // settings
        this.verticesSkipStep = 1; //2
        this.maxAxonDist = all_settings.network_settings.AxonDistance; //default 8
        //this.maxAxonDist = all_settings.network_settings.AxonDistanceInhibitor; //default 4
        this.maxConnectionPerNeuron = all_settings.network_settings.NeuronConnection; //default 6
        //this.maxConnectionPerNeuronInhibitor = all_settings.network_settings.NeuronConnectionInhibitor; //default 20

        this.firing_threshold = all_settings.network_settings.firing_threshold; // threshold to fire signal (not used yet)

        this.currentMaxSignals = 8000;
        this.limitSignals = 200000;

        this.signalMinSpeed = 0.035;
        this.signalMaxSpeed = 0.065;

        // NN component containers
        this.allNeurons = [];
        this.allSignals = [];
        this.allAxons = [];

        
        // neuron
        this.neuronSize = 0.7;
        this.neuronOpacity = 1.0;
        if (scene)
        {
            this.particlePool = new particlePool.ParticlePool(this.limitSignals); // *************** ParticlePool must bigger than limit // axon
            this.excitorAxonDescription = new descriptions.AxonsDescription(0x0099ff);
            this.inhibitorAxonDescription = new descriptions.AxonsDescription(0xff5757);
            this.excitorNeuronDescription = new descriptions.NeuronsDescription(this, 0x00ffff);
            this.inhibitorNeuronDescription = new descriptions.NeuronsDescription(this, 0xff0037);
        }



        // info api
        this.numNeurons = 0;
        this.numAxons = 0;
        this.numSignals = 0;
        // probably shouldn't be hardcoded
        this.numActiveAstrocytes = 7241;
        this.regenSign = 1;
        this.lastRegenUpdate = 0;
        this.lastNeuronDecay = 0;
        //connectivity matrix between different regions
        this.connectivityMatrix =[];

        // initialize NN
        this.initNeuralNetwork();


    }
    NeuralNetwork.prototype.updateRegeneration = function(currentTime) {
    all_settings.astrocyte_settings.updateRegeneration(currentTime, this.logger);
    }


    NeuralNetwork.prototype.constructNeuralNetwork = function(loadedObject){
        var loadedMesh = loadedObject.children[0];
        var vertices = loadedMesh.geometry.vertices;

        this.initNeurons(vertices);
        this.connectNeurons();
        this.initialized = true;
        console.log('Neural Network initialized');
    };

    NeuralNetwork.prototype.buildVisualization = function(loadedObject)
    {
        var loadedMesh = loadedObject.children[0];

        // render loadedMesh
        loadedMesh.material = new THREE.MeshBasicMaterial({
            transparent: true,
            opacity: 0.05,
            depthTest: false,
            color: 0x0088ff,
            blending: THREE.AdditiveBlending
        });
        this.scene.add(loadedObject);
        this.createNeuronVisualization();
        this.createAxonsVisualization();
        this.createParticleVisualization();
        document.getElementById('loading').style.display = 'none'; // hide loading animation when finish loading model
        //this.regenerationFunction();
        //this.decayFunction();
    }; 

    NeuralNetwork.prototype.initNeuralNetwork = function() {
        this.logger.createLogs();
        this.logger.logSettings(all_settings);
        this.initializer.initialize();
    }

    NeuralNetwork.prototype.printRegions = function(){
        for(var i = 0; i<this.allNeurons.length; i++){
            var n = this.allNeurons[i];
            console.log("neuron # "+i+" region: "+n.region+"position: "+n.xPos+" "+n.yPos+" "+n.zPos);
        }
    };

    NeuralNetwork.prototype.randomPosition = function(pos)
    {
        var answer = {}
        var keys = ["x", "y", "z"];
        for (var i = 0; i < 3; i++)
        {
            var k = keys[i];
            answer[k] = (Math.random()*18)-9 + pos[k];
        }
        return answer;
    };

    NeuralNetwork.prototype.initNeurons = function(inputVertices) {
        console.log("init Neurons");
        var numInhibitors = 0;
        var idx = 0;
        for (var i = 0; i < inputVertices.length; i += this.verticesSkipStep) {
            for(var q = 0; q<40; q++){
                var pos = this.randomPosition(inputVertices[i]);
                var excitor = true;
                //every 5th neuron is inhibitory
                if (q % (5*this.verticesSkipStep) == 0) {
                    excitor = false;
                    numInhibitors++;
                }
                if (all_settings.network_settings.izhikevich != 0){
                    var n = new model.IzhikevichNeuron(all_settings, pos.x, pos.y, pos.z, excitor, idx);
                } else {
                    var n = new model.Neuron(all_settings, pos.x, pos.y, pos.z, excitor, idx);
                }
                idx = idx + 1;
                n.region = i+1;

                this.allNeurons.push(n);
            }
        }

        this.numberExcite = this.allNeurons.length - numInhibitors;
        this.numberInhibit = numInhibitors;
 
        for(var i = 0; i < this.allNeurons.length; i++){
            n = this.allNeurons[i];
            this.logger.logRegion(i+1, n.region);
        }
        this.logger.flushAll();
        //this.printRegions();

    };

    NeuralNetwork.prototype.connectNeurons = function () { 
        self = this;
        var allNeuronsLength = this.allNeurons.length;
        for (var j=0; j<allNeuronsLength; j++) {
            var n1 = this.allNeurons[j];
            this.allNeurons.slice(j+1).forEach(function(n2){
                // connect neuron if distance ... and limit connection per neuron to not more than x
                n1.tryConnect(n2, this);
            }, this);
        }
        this.logger.flushAll();
    };

    NeuralNetwork.prototype.populateAxonDescriptions = function()
    {
        this.allAxons.forEach(function(axon){
            if( axon.type === model.INHIBITOR)
                this.inhibitorAxonDescription.constructArrayBuffer(axon.axonVisualization);
            else
                this.excitorAxonDescription.constructArrayBuffer(axon.axonVisualization);
        }, this);
    }

 NeuralNetwork.prototype.populateNeuronDescriptions = function()
    {
        this.allNeurons.forEach(function(neuron){
            if( neuron.type === model.INHIBITOR)
                this.inhibitorNeuronDescription.push(neuron);
            else
                this.excitorNeuronDescription.push(neuron);
        }, this);
    }

    NeuralNetwork.prototype.createAxonsVisualization = function () {
        this.populateAxonDescriptions();
        this.excitorAxonMesh = this.excitorAxonDescription.mesh();
        this.inhibitorAxonMesh = this.inhibitorAxonDescription.mesh();
        this.scene.add(this.excitorAxonMesh);
        this.scene.add(this.inhibitorAxonMesh);
    };

    NeuralNetwork.prototype.createNeuronVisualization = function ()
    {
        this.populateNeuronDescriptions();
        this.excitorParticles = this.excitorNeuronDescription.mesh();
        this.inhibitorParticles = this.inhibitorNeuronDescription.mesh();
        this.scene.add(this.excitorParticles);
        this.scene.add(this.inhibitorParticles);
    };

    NeuralNetwork.prototype.createParticleVisualization = function ()
    {
        this.particlePool.buildVisualization(this.scene);
    };



    NeuralNetwork.prototype.update = function(currentTime) {
        if (currentTime == 100000){
                this.logger.flushAll()

        }
        if (!this.initialized) return;
        this.updateRegeneration(currentTime);

        if (this.lastNeuronDecay + all_settings.network_settings.decayTime < currentTime)
        {
            this.allNeurons.forEach(function(n){n.decay();});
            this.lastNeuronDecay = currentTime
        }

        var n, ii;

        // update neurons state and release signal
        for (ii = 0; ii < this.allNeurons.length; ii++) {
            n = this.allNeurons[ii];
            // the astrocyte we're taking energy from
            var result = n.step(this, currentTime);
                if (result[0] === true)
                    this.logger.logFiring(currentTime, ii+1, result[1]);
                else if (result[1] != null)
                    this.logger.logMissEnergy(currentTime, ii+1, result[1]);
        }
        for (ii = 0; ii < this.allNeurons.length; ii++) {
            n = this.allNeurons[ii];
            n.replenishAstrocyte(currentTime);
        }       

        self = this;
        this.allSignals.map(function(signal){
            signal.travel(currentTime, self.logger);
            if (!signal.alive)
                signal.freeParticle();
        });
        this.allSignals = this.allSignals.filter(function(signal)
        {
            return signal.alive;
        });


        if (this.browser)// update particle pool vertices
            this.particlePool.update();

        // update info for GUI
        this.updateInfo(currentTime);

    };

    NeuralNetwork.prototype.releaseSignalAt = function(neuron) {
        var signals = neuron.createSignal(this.particlePool, this.signalMinSpeed, this.signalMaxSpeed);
        for (var ii = 0; ii < signals.length; ii++) {
            var s = signals[ii];
            this.allSignals.push(s);
        }
    };

    NeuralNetwork.prototype.updateInfo = function(currentTime) {
        this.numNeurons = this.allNeurons.length;
        this.numAxons = this.allAxons.length;
        this.numSignals = this.allSignals.length;
        var activeAstros = 0;
        var totalEnergy = 0;
        for (var i = 0; i < this.numNeurons; i++) {
            var astrocyte = this.allNeurons[i].astrocyte;
            totalEnergy += astrocyte.availableEnergy;
            if (astrocyte.hasEnergy())
                activeAstros++;
        }
        this.numActiveAstrocytes = activeAstros;

        this.logger.logEnergy(currentTime, activeAstros, totalEnergy);

    };

    NeuralNetwork.prototype.updateSettings = function() {
        this.inhibitorNeuronDescription.updateSettings();
        this.excitorNeuronDescription.updateSettings()

        this.excitorAxonDescription.updateSettings();
        this.inhibitorAxonDescription.updateSettings();

        this.particlePool.updateSettings();
    };

    var all_settings = {
        astrocyte_settings : {
            minEnergy: 0.0, // default min
            maxEnergy: 1.0, // default max
            fireEnergy: 0.125, // the amount that depletes on firing
            replenishEnergy: 0.08, // amount of energy astrocyte regenerates
            regenerationTime: 150, // time needed for energy to regenerate in milliseconds
            //minThreshold: 0.125, // energy level at which the astrocyte starts regenerating energy
            minThreshold: 0.02, //
            maxThreshold: 0.08, //
            frequency: 100, // in milliseconds
            amplitude: 0.01, // increased by this amount
            regenSign: 1,
            lastRegenUpdate : 0
        },
        network_settings : {
            izhikevich: true, //anything but 0 for izhikevich neuron model
            firing_threshold: 0.60, // neuron fires when reaching this amount.
            signal_weight: 0.40, // energy of neuron increases by this amount per signal.
            AxonDistance: 10, //default
            //AxonDistanceInhibitor: 4, //default
            NeuronConnection: 6, //default
            //NeuronConnectionInhibitor: 20, //default
            decayTime: 50, //time needed for neurons to decay,
            shouldLog: false,

    }};

    function inject_behavior_to_settings(settings)
    {
        settings.network_settings['reload'] = function() {
                window.clock = new Clock();
                window.scene = new THREE.Scene();
                var nn = new NeuralNetwork(window.scene, window.clock);
                window.neuralNet = nn;
                var to_delete = window.to_delete;
                window.to_delete = window.init_gui(window.gui, nn, all_settings, to_delete);
            };
        var self = settings.astrocyte_settings;
        settings.astrocyte_settings["updateRegeneration"] = function(currentTime, logger) {
            var timeSinceLastUpdate = currentTime - self.lastRegenUpdate;
            if(timeSinceLastUpdate >= self.frequency){
                var replenish = (self.replenishEnergy += self.regenSign*self.amplitude);
                self.lastRegenUpdate = currentTime;
                var up = replenish > self.maxThreshold;
                var down = replenish < self.minThreshold;
                if (up || down)
                {
                    self.regenSign *= -1;
                    if (up)
                      self.replenishEnergy = self.maxThreshold;
                    else
                      self.replenishEnergy = self.minThreshold;
                }
            }};
    };
    inject_behavior_to_settings(all_settings);



  global.Clock = Clock;
  global.NeuralNetwork = NeuralNetwork;
  global.all_settings = all_settings;
})(neuralNetwork);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.neuralNetwork = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(neuralNetwork);


console.log(neuralNetwork);
(function main(){
    var clock = new neuralNetwork.Clock();
    var neuralNet = new neuralNetwork.NeuralNetwork();
    var f;

    f = function(){
        if (neuralNet.initialized)
            evolve(clock, neuralNet)
        else
            setTimeout(f, 10);
    };
    f();
}());

function evolve(clock, neuralNet)
{
    var time = clock.time();
    while (time < 100000){        
        neuralNet.update(time);
        if ((time % 1000) == 0)
        {
            process.stdout.write("Running... Age: " + time + "ms\r");
        }
        time = clock.inc(1);
    }
}
