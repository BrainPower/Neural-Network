var descriptions = descriptions || {};

(function(global) {
  "use strict";

	function NeuronsDescription(neuralNet, color)
	{
		this.neuralNet = neuralNet;
		this.spriteTexture = THREE.ImageUtils.loadTexture("./static/sprites/electric.png");
		this.color = color;
		this.geom = new THREE.Geometry();
		this.material = new THREE.PointCloudMaterial({
			map: this.spriteTexture,
			blending: THREE.AdditiveBlending,
			depthTest: false,
			transparent: true,
			color: {
				type: 'c',
				value: new THREE.Color(this.color)
			}
		});
		this.updateSettings();
	}; 

	NeuronsDescription.prototype.updateSettings = function() {
		this.material.opacity = this.neuralNet.neuronOpacity;
		this.material.color.set(this.color);
		this.material.size = this.neuralNet.neuronSize;
	};
	NeuronsDescription.prototype.push = function(neuron)
	{
		this.geom.vertices.push(neuron);
	}

	NeuronsDescription.prototype.mesh = function()
	{
		return new THREE.PointCloud(this.geom, this.material);
	};

	function AxonsDescription(color)
	{
		this.opacityMultiplier = 1.0;
		this.color = color; // excitor: 0x0099ff, inhibitor: 0xff5757
		this.geom = new THREE.BufferGeometry();
		this.positions = [];
		this.indices = [];
		this.nextPositionsIndex = 0;

		this.shaderUniforms = {
			color: {
				type: 'c',
				value: new THREE.Color(this.color)
			},
			opacityMultiplier: {
				type: 'f',
				value: 1.0
			}
		};

		this.shaderAttributes = {
			opacityAttr: {
				type: 'f',
				value: []
			}
		};
	}


	AxonsDescription.prototype.constructArrayBuffer = function(axon)
	{
		var vertices = axon.geom.vertices;
		var numVerts = vertices.length;

		for (var i = 0; i < numVerts; i++) {
			this.positions.push(vertices[i].x, vertices[i].y, vertices[i].z);
			if (i < numVerts - 1) {
				var idx = this.nextPositionsIndex;
				this.indices.push(idx, idx + 1);

				var opacity = THREE.Math.randFloat(0.002, 0.2);
				this.shaderAttributes.opacityAttr.value.push(opacity, opacity);
			}
			this.nextPositionsIndex += 1;
		}		
	}

	AxonsDescription.prototype.updateSettings = function()
	{
		this.shaderUniforms.color.value.set(this.color);
		this.shaderUniforms.opacityMultiplier.value = this.opacityMultiplier;
	};

	AxonsDescription.prototype.mesh = function() 
	{
		var indices = new Uint32Array(this.indices.length);
		var positions = new Float32Array(this.positions.length);
		var opacities = new Float32Array(this.shaderAttributes.opacityAttr.value.length);

		// transfer temp-array to arrayBuffer
		transferToArrayBuffer(this.indices, indices);
		transferToArrayBuffer(this.positions, positions);
		transferToArrayBuffer(this.shaderAttributes.opacityAttr.value, opacities);
		this.indices = [];
		this.positions = [];
		this.opacities = [];

		function transferToArrayBuffer(fromArr, toArr) {
			for (var i=0; i<toArr.length; i++) {
				toArr[i] = fromArr[i];
			}
		}

		this.geom.addAttribute( 'index', new THREE.BufferAttribute(indices, 1) );
		this.geom.addAttribute( 'position', new THREE.BufferAttribute(positions, 3) );
		this.geom.addAttribute( 'opacityAttr', new THREE.BufferAttribute(opacities, 1) );


		// axons mesh
		this.shaderMaterial = new THREE.ShaderMaterial( {
			uniforms:       this.shaderUniforms,
			attributes:     this.shaderAttributes,
			vertexShader:   document.getElementById('vertexshader-axon').textContent,
			fragmentShader: document.getElementById('fragmentshader-axon').textContent,
			blending:       THREE.AdditiveBlending,
			// depthTest:      false,
			transparent:    true
		});

		this.mesh = new THREE.Line(this.geom, this.shaderMaterial, THREE.LinePieces);
		return this.mesh;
	};

	global.NeuronsDescription = NeuronsDescription;
	global.AxonsDescription = AxonsDescription;
})(descriptions);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.descriptions = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(descriptions);