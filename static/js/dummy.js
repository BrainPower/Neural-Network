
console.log(neuralNetwork);
(function main(){
    var clock = new neuralNetwork.Clock();
    var neuralNet = new neuralNetwork.NeuralNetwork();
    var f;

    f = function(){
        if (neuralNet.initialized)
            evolve(clock, neuralNet)
        else
            setTimeout(f, 10);
    };
    f();
}());

function evolve(clock, neuralNet)
{
    var time = clock.time();
    while (time < 100000){        
        neuralNet.update(time);
        if ((time % 1000) == 0)
        {
            process.stdout.write("Running... Age: " + time + "ms\r");
        }
        time = clock.inc(1);
    }
}
