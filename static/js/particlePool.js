var particlePool = particlePool || {};

(function(global) {
  "use strict";

    function ParticlePool(poolSize) {

        this.spriteTextureSignal = THREE.ImageUtils.loadTexture("./static/sprites/electric.png");

        this.poolSize = poolSize;
        this.pGeom = new THREE.Geometry();
        this.particles = this.pGeom.vertices;
        this.availableParticles = []

        this.offScreenPos = new THREE.Vector3(9999, 9999, 9999); // #CM0A r68 PointCloud default frustumCull = true(extended from Object3D), so need to set to 'false' for this to work with oppScreenPos, else particles will dissappear

        // this.pColor = 0xfff6cd;
        this.pColor = 0xff4400;
        this.pSize = 0.6;

        var particle;
        for (var ii = 0; ii < this.poolSize; ii++) {
            particle = new Particle(this);
            this.particles[ii] = particle;
            this.free(particle)
        }

    };

    ParticlePool.prototype.buildVisualization = function(scene)
    {
        // inner particle
        this.pMat = new THREE.PointCloudMaterial({
            map: this.spriteTextureSignal,
            size: this.pSize,
            color: this.pColor,
            blending: THREE.AdditiveBlending,
            depthTest: false,
            transparent: true
        });

        this.pMesh = new THREE.PointCloud(this.pGeom, this.pMat);
        this.pMesh.frustumCulled = false; // ref: #CM0A

        scene.add(this.pMesh);


        // outer particle glow
        this.pMat_outer = new THREE.PointCloudMaterial({
            map: this.spriteTextureSignal,
            size: this.pSize * 10,
            color: this.pColor,
            blending: THREE.AdditiveBlending,
            depthTest: false,
            transparent: true,
            opacity: 0.025
        });

        this.pMesh_outer = new THREE.PointCloud(this.pGeom, this.pMat_outer);
        this.pMesh_outer.frustumCulled = false; // ref:#CM0A

        scene.add(this.pMesh_outer);

    };

    ParticlePool.prototype.getParticle = function() {
        if (this.availableParticles.length > 0)
            return this.availableParticles.pop();
        return null;

    };

    ParticlePool.prototype.free = function(particle){
        particle.set(this.offScreenPos.x, this.offScreenPos.y, this.offScreenPos.z);
        this.availableParticles.push(particle);
    };

    ParticlePool.prototype.update = function() {

        this.pGeom.verticesNeedUpdate = true;

    };

    ParticlePool.prototype.updateSettings = function() {

        // inner particle
        this.pMat.color.setHex(this.pColor);
        this.pMat.size = this.pSize;
        // outer particle
        this.pMat_outer.color.setHex(this.pColor);
        this.pMat_outer.size = this.pSize * 10;

    };

    // Particle --------------------------------------------------------------
    // Private class for particle pool

    function Particle(particlePool) {

        this.particlePool = particlePool;

    }

    Particle.prototype = Object.create(THREE.Vector3.prototype);

    Particle.prototype.free = function() {
        this.particlePool.free(this);

    };

    global.ParticlePool = ParticlePool;
    global.Particle = Particle;
})(particlePool);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.particlePool = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(particlePool);
