var logger = logger || {};

(function(global) {
  "use strict";
    function LogType(logger, url, directory, softlimit)
    {
        this.logger = logger;
        this.directory = directory;
        this.url = url;
        this.store = [];
        this.softlimit = softlimit || 1000;
    };

    LogType.prototype.log = function(objects) {
        this.store.push(objects.join(",") + "\n");
        if(this.store.length >= this.softlimit){
            this.flush();
        }
    };

    LogType.prototype.flush = function(){
        if(this.store.length > 0){
            this.logger.sendToServer(this.store, this.url);
            this.store = [];
        }
    };

    // Logger ----------------------------------------------------------------
    function Logger(fileclass){
        this.fileHandle = new fileclass(this);
        this.firing = new LogType(this, "firing", "Firing");
        this.input = new LogType(this, "input", "Input");
        this.potential  = new LogType(this, "potential", "Potential");
        this.missed = new LogType(this, "miss", "MissEnergy");
        this.replenish  = new LogType(this, "replenish", "ReplenishEnergy", 20);
        this.connections  = new LogType(this, "connection", "Connections");
        this.weights = new LogType(this, "conweights", "ConWeights");
        this.regions = new LogType(this, "region", "Region", 200);
        this.energy = new LogType(this, "energy", "Energy");
        this.settings = new LogType(this, "settings", "Settings", 1);
        this.all = [this.firing, this.input, this.potential, this.missed,
                    this.replenish, this.connections, this.weights, 
                    this.regions, this.energy, this.settings];
    }

    Logger.prototype.logFiring = function(time, neuron, potential){
        this.firing.log([time, neuron, 1]);
        this.potential.log([time, neuron, potential]);
    }
    Logger.prototype.logInput = function(time, from, to, type){
        this.input.log([time, from.idx, to.idx, type]);
    }
    Logger.prototype.logMissEnergy = function(time, neuron, energy){
        this.missed.log([time, neuron, energy]);
    }
    Logger.prototype.logRep = function(time, energy){
        this.replenish.log([time, energy]);
    }

    Logger.prototype.logEnergy = function(time, astrocytes, totalEnergy){
        this.energy.log([time, astrocytes, totalEnergy]);
    }
    Logger.prototype.logCon = function(n1, n2, weight){
        this.connections.log([n1, n2, 1]);
        this.weights.log([n1, n2, weight])
    }
    Logger.prototype.logRegion = function(neuron, region){
        this.regions.log([neuron, region])
    }

    Logger.prototype.logSettings = function(settings)
    {
        this.settings.log([JSON.stringify(settings),]);
    }

    Logger.prototype.flushAll = function(){
        for (var i = 0; i < this.all.length; i++)
            this.all[i].flush();
    }
    Logger.prototype.sendToServer = function(entries, url){
        this.fileHandle.sendToServer(entries, url);
    }
    Logger.prototype.createLogs = function(){
        this.fileHandle.createLogs();
    }


    function AjaxFiles(logger)
    {};


    AjaxFiles.prototype.sendToServer = function(entries, url){
        $.ajax({
            type: "POST",
            url: "/"+url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(entries),
            dataType: 'json',
            success: function(data) {
//              console.log("success");
            },
            error: function(error) {
              console.log(error);
            }
        });
    }
    AjaxFiles.prototype.createLogs = function(){
        $.ajax({
            type: "POST",
            url: "/createLogs",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify("Create the logs"),
            dataType: 'json',
            success: function(data) {
//              console.log("success");
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function NullFiles(logger)
    {};

    NullFiles.prototype.sendToServer = function(entries, url){}
    NullFiles.prototype.createLogs = function(){}

    function NodeFiles(logger)
    {
        this.logger = logger;
        this.files = {};
    };

    NodeFiles.prototype.sendToServer = function(entries, url){
        var fd = this.files[url];
        fs.write(fd, entries.join(''));
    };
    NodeFiles.prototype.twoDigits = function(n)
    {
        return ("0" + n).slice(-2);
    };

    NodeFiles.prototype.buildTimestamp = function()
    {
        var date = new Date();
        var year = date.getFullYear().toString();
        var month = (date.getMonth() + 1).toString();
        var day = date.getDate().toString();
        var hours = date.getHours().toString();
        var minutes = date.getMinutes().toString();
        var seconds = date.getSeconds().toString();
        var first = [year, this.twoDigits(month), this.twoDigits(day)].join('-');
        var second = [hours, minutes, seconds].map(this.twoDigits).join('-');
        this.timestamp = first + '_' + second;        
    }
    NodeFiles.prototype.createLogs = function(){
        this.buildTimestamp();
        var baseDirectory = '../BrainPowerLogs';
        var self = this;
        this.logger.all.forEach(function(logType){
            var directory = baseDirectory + '/' + logType.directory;
            mkdirp.sync(directory);
            self.files[logType.url] = fs.openSync(directory + '/' + self.timestamp, 'w');
        });

    };    

    global.Logger = Logger;
    global.NullFiles = NullFiles;
    global.AjaxFiles = AjaxFiles;
    global.NodeFiles = NodeFiles;
})(logger);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.logger = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(logger);

