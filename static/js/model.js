var model = model || {};

(function(global) {
  "use strict";
	const EXCITOR = 0;
	const INHIBITOR = 1;

	// Neuron ----------------------------------------------------------------

	function Neuron(all_settings, x, y, z, excitor, idx) {
        this.settings = all_settings.network_settings;

    	this.type = excitor ? EXCITOR : INHIBITOR;
    	this.connection = [];
    	// represents the 1:1 ratio astrocyte associated w/ neuron
        this.astrocyte = new model.Astrocyte(all_settings.astrocyte_settings);
        this.reset(0);
    	// the neurons this neuron is connected to
    	this.neurons = [];
    	this.lastSignalRelease = 0;
    	this.releaseDelay = 0;
    	this.prevReleaseAxon = null;
    	this.refactoryPeriod = 10;
    	this.xPos = x;
    	this.yPos = y;
    	this.zPos = z;
    	this.idx = idx + 1;

    	//neuron fires when this number passes the firing threshold
    	this.acc = -0.5;

    	this.region = -1; // which region the neuron belongs to
    	// 1 - 188, assigned during initialization

    	THREE.Vector3.call(this, x, y, z);
	}

	Neuron.prototype = Object.create(THREE.Vector3.prototype);

	//One directional connection from the neuron calling the method to the neuron in the parameter.
	Neuron.prototype.connectNeuronOneDirection = function(neuronB) {

		var neuronA = this;
		var type = neuronA.type;
		// create axon and establish connection from A to B
		var axon = new Axon(neuronA, neuronB, type);
		neuronA.connection.push(new Connection(axon, 'A'));
		neuronA.neurons.push(neuronB);
		return axon;

	};

	//Two directional connection from the neuron calling the method to the neuron in the parameter.
	//A signal can travel both ways
	Neuron.prototype.connectNeuronTwoDirection = function(neuronB) {

		var neuronA = this;
		var type = neuronA.type;
		// create axon and establish connection in both directions
		var axon = new Axon(neuronA, neuronB, type);
		neuronA.connection.push(new Connection(axon, 'A'));
		neuronB.connection.push(new Connection(axon, 'B'));
		neuronA.neurons.push(neuronB);
		neuronB.neurons.push(neuronA);
		return axon;

	};

    Neuron.prototype.resetPotentialEnergy = function() {
        this.acc = 0.125-1; //resets energy of neuron
    };

    Neuron.prototype.reset = function(currentTime) {
        this.resetPotentialEnergy();
        this.releaseDelay = 0;
        this.fired = false;
        this.receivedSignal = false;
        this.firedCount = 0;
        this.signalCount = 0;
        // reset all astrocytes
        // TODO: in the future this should be adjustable and occur constantly,
        // not just when the signal dies.
        this.astrocyte.resetEnergy(currentTime);        
    }

	Neuron.prototype.decay = function(){
        if (this.astrocyte.hasEnergy())
        {
    		var epsilon = -0.0001;
    		this.acc = 0.9 * this.acc;
    		if (this.acc > epsilon)
    			this.acc = 0;
        }
	}

    Neuron.prototype.canConnectTo = function(n2, network) {
        var r1 = parseInt(this.region);
        var r2 = parseInt(n2.region);

        if (r1 ===r2 && this.distanceTo(n2) < network.maxAxonDist)
            return true;
        else
        {
            var probFromMatrix = network.connectivityMatrix[r1-1][r2];
            probFromMatrix = Number(probFromMatrix);
            probFromMatrix = probFromMatrix / 5000;
            return Math.random() < probFromMatrix;
        }
    };
	Neuron.prototype.tryConnect = function(neuronB, network){
		var n1 = this;
		var n2 = neuronB;

        if (n1 === n2)
            return;

        if (!n1.canConnectTo(n2, network))
            return;

		var rand = Math.floor( Math.random() * 3 );

		//two directional connection
		if(rand === 0)
			var connectedAxon = n1.connectNeuronTwoDirection(n2);
	 	//one directional connection n2 to n1
		else if(rand === 1)
			var connectedAxon = n2.connectNeuronOneDirection(n1);
		//one directional connection n1 to n2
		else if(rand === 2)
			var connectedAxon = n1.connectNeuronOneDirection(n2);
		network.allAxons.push(connectedAxon);
		var rand = (Math.random()*41+80)/100;
		connectedAxon.weight = rand * (1/connectedAxon.cpLength);
		if(network.logger != null){
			network.logger.logCon(n1.idx, n2.idx, connectedAxon.weight);
			if(rand === 0)
				network.logger.logCon(n2.idx, n1.idx, connectedAxon.weight);
		}
		
	}

	Neuron.prototype.createSignal = function(particlePool, minSpeed, maxSpeed) {

		this.firedCount += 1;
		this.receivedSignal = false;

		// create signal to all connected axons
		return this.connection.filter(function(connection)
			{
				return connection.axon !== this.prevReleaseAxon;
			}, this)
			.map(function(connection)
			{
				return new Signal(connection, particlePool, minSpeed, maxSpeed);
			})
	};


	//returns active astrocyte, it's taking energy from
	Neuron.prototype.astrocyteWithEnergy = function(neighbors) {
		neighbors = typeof neighbors !== 'undefined' ? neighbors : true;
		var total = this.neurons.length;
		//console.log(" i"+this.neurons.length);
		var activeAstrocyte = null;
		// see if the astrocyte directly linked to this neuron has the energy needed to fire
		if (this.astrocyte.hasEnergy() === true) {
			return this.astrocyte;
		}

		// if we get here, the directly linked astrocyte did not have enough energy
		// check the astrocytes of surrounding neurons to see if they have enough energy
		if (neighbors){
			for (var i = 0; i < total; i++) {
				var astrocyte = this.neurons[i].astrocyteWithEnergy(false);
				if (astrocyte == null)
					return astrocyte;
			}
		}
		return null;

	};
	Neuron.prototype.willFire = function(currentTime)
	{
		var timeSinceLastFiring = currentTime - this.lastSignalRelease; // used to check if in refactory period
		if (timeSinceLastFiring >= this.refactoryPeriod && //past refactory period
			this.acc >= this.settings.firing_threshold-1) //has to be past the threshold to consider firing
				return Math.random() < 1+this.acc;
		else
			return false;
	}
	//neuron firing function with probability of firing equal to the energy level
	Neuron.prototype.fire = function() {
		this.fired = true;
		this.resetPotentialEnergy();
		// decrease energy level of astrocyte responsible for
		// giving the neuron the energy it needed to fire
		this.releaseDelay = THREE.Math.randInt(100, 1000);
	};

	Neuron.prototype.step = function(neuralNet, currentTime)
	{
		var ret = [false, null];
		if (this.willFire(currentTime))
		{
			var astrocyte = this.astrocyteWithEnergy(false);
			if (astrocyte != null) { // Astrocyte mode
				var prevacc = this.acc;
				this.fire();
				ret = [true, prevacc]
				astrocyte.deplete(currentTime);
				this.lastSignalRelease = currentTime;
				neuralNet.releaseSignalAt(this);
			} else {
				ret = [false, this.astrocyte.availableEnergy];
			}
		}
		return ret;
	}
	Neuron.prototype.replenishAstrocyte = function(currentTime){
		this.astrocyte.replenish(currentTime);
	}
	Neuron.prototype.effectiveSignal = function() {
		return (this.prevReleaseAxon.weight * this.settings.signal_weight);
	}

	//accumulation function when recieving a signal from an excitory neuron
	Neuron.prototype.buildExcitor = function() {
		this.acc = Math.min(0, this.acc + this.effectiveSignal());
	};

	//accumulation function when recieving a signal from an inhibitory neuron
	Neuron.prototype.buildInhibitor = function() {
		this.acc = Math.max(-1, this.acc - this.effectiveSignal());
	};

    function IzhikevichNeuron(all_settings, x, y, z, excitor, idx){
        Neuron.call(this, all_settings, x, y, z, excitor, idx);
        var r = Math.random();
        this.a = 0.02 + (this.type == INHIBITOR ? 0.08 * r: 0);
        this.b = this.type == INHIBITOR ? 0.25 - 0.05*r : 0.2;
        this.c = -65 + (this.type == INHIBITOR ? 0 : 15*r*r);
        this.d = this.type == INHIBITOR ? 2 : 8-6*r*r;
        this.acc = -65;
        this.u = this.b*this.acc;
    };

    IzhikevichNeuron.prototype = Object.create(Neuron.prototype);


    IzhikevichNeuron.prototype.willFire = function(currentTime)
    {
        var timeSinceLastFiring = currentTime - this.lastSignalRelease; // used to check if in refactory period
        if (timeSinceLastFiring >= this.refactoryPeriod )
            return this.acc >= 30;
    }

    IzhikevichNeuron.prototype.step = function(neuralNet, currentTime)
    {
        var answer = Neuron.prototype.step.call(this, neuralNet, currentTime);
        if ((this.acc <= 30) & (this.astrocyte.hasEnergy()))
        {
            var r = Math.random();
            var thalamic_input = this.type == INHIBITOR ? 2 * r: 5 * r;
            var v = this.acc;
            this.acc = v + 0.5 * (0.04 * v * v + 5 * v + 140 - this.u + thalamic_input);
            var v = this.acc;
            this.acc = v + 0.5 * (0.04 * v * v + 5 * v + 140-this.u + thalamic_input);
            this.u = this.u + this.a*(this.b*this.acc - this.u)
        }
        return answer;
    }

    IzhikevichNeuron.prototype.decay = function(){};

    IzhikevichNeuron.prototype.resetPotentialEnergy = function(currentTime)
    {
        this.acc = this.c;
        this.u = this.u + this.d;
    }


    //accumulation function when recieving a signal from an excitory neuron
    IzhikevichNeuron.prototype.buildExcitor = function() {
        this.acc = this.acc + this.effectiveSignal();
    };

    //accumulation function when recieving a signal from an inhibitory neuron
    IzhikevichNeuron.prototype.buildInhibitor = function() {
        this.acc = this.acc - this.effectiveSignal();
    };


	// Astrocyte -------------------------------------------------------------
	function Astrocyte(settings) {
		// replaces the if firedCount < 8
    this.settings = settings;
		this.availableEnergy = this.settings.maxEnergy;
		this.lastRefilled = 0;
	}

	// TODO: Get rid of this because we should never have a situation where the astrocyte energy is hard-reset, right?...
	// Astrocytes should just regenerate energy at a constant rate and neurons pull from it if it's there and they need it...
	Astrocyte.prototype.resetEnergy = function(currentTime) {
		//this.availableEnergy = THREE.Math.randInt(this.settings.minEnergy, this.settings.maxEnergy);
        this.availableEnergy += this.settings.replenishEnergy;
		if (this.availableEnergy > this.settings.maxEnergy)
			this.availableEnergy = this.settings.maxEnergy;
		//console.log("reset: "+ this.availableEnergy);
		this.lastRefilled = currentTime;
	};
	Astrocyte.prototype.hasEnergy = function(){
		if(this.availableEnergy >= this.settings.fireEnergy)
			return true;
		else
			return false;
	}
	//depletes energy from the astrocyte when the neuron fires
	Astrocyte.prototype.deplete = function(currentTime) {
		this.availableEnergy -= this.settings.fireEnergy; //energy needed to fire a signal
		if (this.availableEnergy <= this.settings.minEnergy) {
			this.availableEnergy = this.settings.minEnergy
		}
		this.lastRefilled = currentTime;
	};

	//regenerates energy of the astrocyte in a certain time period that can be set in the settings
	Astrocyte.prototype.replenish = function(currentTime) {
		if (currentTime - this.lastRefilled> this.settings.regenerationTime)
		{
			this.availableEnergy += this.settings.replenishEnergy;
			if(this.availableEnergy > this.settings.maxEnergy){
				this.availableEnergy = this.settings.maxEnergy;
			}
			this.lastRefilled = currentTime;
		}
	};

	// Signal ----------------------------------------------------------------

	function Signal(connection, particlePool, minSpeed, maxSpeed) {

		this.minSpeed = minSpeed;
		this.maxSpeed = maxSpeed;
		this.speed = THREE.Math.randFloat(this.minSpeed, this.maxSpeed);
		this.alive = true;
		this.t = null;
		this.startingPoint = null;
		this.axon = null;
		this.particle = particlePool ? particlePool.getParticle() : null;
		this.excitor = true;
		THREE.Vector3.call(this);
		this.setConnection(connection);

	}

	Signal.prototype = Object.create(THREE.Vector3.prototype);

	Signal.prototype.setConnection = function(Connection) {

		this.startingPoint = Connection.startingPoint;
		this.axon = Connection.axon;
		if (this.startingPoint === 'A') this.t = 0;
		else if (this.startingPoint === 'B') this.t = 1;

	};

	Signal.prototype.dispatchSignal = function(from, to, clock, logger)
	{
		this.alive = false;
		to.receivedSignal = true;
		to.signalCount++;
		to.prevReleaseAxon = this.axon;
		//checks what type of neuron sent the signal to call the correct build function
		if (from.type == EXCITOR){
			to.buildExcitor();
			if(logger != null)
				logger.logInput(clock, from, to, EXCITOR);
		}
		else if (from.type == INHIBITOR) {
			//console.log("firer = "+this.axon.neuronA.type+" reciever = "+this.axon.neuronB.type);
			//console.log("energy before = "+this.axon.neuronB.acc);
			to.buildInhibitor();
			if(logger != null)
				logger.logInput(clock, from, to, INHIBITOR);
		}

	}

	Signal.prototype.freeParticle = function(){
		if (this.particle != null)
		{
			this.particle.free();
			this.particle = null;
		}
	}

	Signal.prototype.travel = function(clock, logger) {

		var pos;
		//var temp = this.axon.getPoint(this.t);
		// console.log("direction of axon = "+this.axon.direction + "starting point = "+this.startingPoint);
		// if (this.startingPoint === 'A' && (this.axon.direction === 0 || this.axon.direction === 2)) {
		if (this.startingPoint === 'A') {
			this.t += this.speed;
			if (this.t >= 1) {
				this.t = 1;
				this.dispatchSignal(this.axon.neuronA, this.axon.neuronB, clock, logger);
			}
			//console.log("fired signal = "+this.startingPoint);

		} //else if (this.startingPoint === 'B' && (this.axon.direction === 1 || this.axon.direction === 2)) {
		else if (this.startingPoint === 'B') {
			this.t -= this.speed;
			if (this.t <= 0) {
				this.t = 0;
				this.dispatchSignal(this.axon.neuronB, this.axon.neuronA, clock, logger);
			}
			//console.log("fired signal = "+this.startingPoint);
		}

		if (this.particle != null)
		{
			pos = this.axon.getPoint(this.t);
			this.particle.set(pos.x, pos.y, pos.z);
		}

	};

  	// Axon ------------------------------------------------------------------
  	function AxonVisualization(axon)
  	{
  		this.axon = axon;
  		var neuronA = axon.neuronA;
  		var neuronB = axon.neuronB;
  		THREE.LineCurve3.call(this, neuronA, neuronB);
  		this.geom = new THREE.Geometry();
  		this.geom.vertices.push(neuronA, neuronB);
  	}
    AxonVisualization.prototype = Object.create(THREE.LineCurve3.prototype);

    function Axon(neuronA, neuronB, type) {

    	this.weight = 1;
    	this.type = type;

    	this.neuronA = neuronA;
    	this.neuronB = neuronB;
    	this.axonVisualization = new AxonVisualization(this);
    	this.cpLength = this.distance();
    }

    Axon.prototype.distance = function()
    {
    	return this.neuronA.distanceTo(this.neuronB);
    }

    Axon.prototype.getPoint = function(t)
    {
    	return this.axonVisualization.getPoint(t);
    }

    // Connection ------------------------------------------------------------
    function Connection(axon, startingPoint) {
    	this.axon = axon;
    	this.startingPoint = startingPoint;


    }


	global.Neuron = Neuron;
	global.Signal = Signal;
    global.Astrocyte = Astrocyte;
    global.EXCITOR = EXCITOR;
    global.INHIBITOR = INHIBITOR;
    global.IzhikevichNeuron = IzhikevichNeuron;

})(model);

(function(lib) {
  "use strict";
  if (typeof module === "undefined" || typeof module.exports === "undefined") {
    window.model = lib; // in ordinary browser attach library to window
  } else {
    module.exports = lib; // in nodejs
  }
})(model);
