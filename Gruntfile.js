module.exports = function ( grunt ) {

    grunt.initConfig( {

        pkg: grunt.file.readJSON( 'package.json' ),

        browserify: {
            build: {
                files: {
                    'js/build/deploy.js': [ 'js/build/app.js' ]
                }
            }
        },
        concat: {
            options: {
                // banner: '/* <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                // footer: ''
            },
            build: {
                src: ['static/js/logger.js',
                        'static/js/model.js', 'static/js/particlePool.js', 'static/js/descriptions.js',
                        'static/js/neuralNetwork.js', 'static/js/dummy.js'],

                dest: 'js/build/no-gui.js'
            },
            vendor: {
                src: [ 'static/js/vendor/dummy_self.js', 'static/js/vendor/jquery.min.js', 'js/vendor/dat.gui.min.js', 'static/js/vendor/stats.min.js', 'static/js/vendor/three.js', 'static/static/js/vendor/Detector.js','static/js/vendor/OrbitControls.js', 'static/js/vendor/OBJLoader.js'],

                dest: 'js/vendor/vendor-merge.js'
            },
            both : {
                src : ['js/vendor/vendor-merge.js', 'js/build/no-gui.js', ],
                dest : 'js/build/deploy-no-gui.js'
            }
        },
        uglify: {
            options: {},
            build: {
                src: [ 'static/js/build/app.js' ],
                dest: 'static/js/build/app.min.js',
                sourceMap: true
            },
            vendor: {
                src: [ 'static/js/vendor/vendor-merge.js' ],
                dest: 'static/js/vendor/vendor-merge.min.js',
                sourceMap: false
            }
        },
        watch: {
            options: { // global opptions for all watchers
                livereload: true
            },
            js: {
                files: 'static/js/*.js',
                tasks: [ 'concat' ]
            },
            html: {
                files: '*.html'
            }
        },
        connect: {
            server: {
                options: {
                    port: 9001,
                    base: '.',
                }
            }
        }

    } );

    // Load the plugin that provides the tasks.
    grunt.loadNpmTasks( 'grunt-browserify' );
    grunt.loadNpmTasks( 'grunt-contrib-concat' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-contrib-connect' );

    // tasks
    grunt.registerTask( 'default', [ 'concat:build', 'concat:vendor', 'concat:both'] );
    grunt.registerTask( 'build', [ 'concat:build', 'uglify:build' ] );
    grunt.registerTask( 'vendor', [ 'concat:vendor', 'uglify:vendor' ] );
    grunt.registerTask( 'deploy', [ 'concat:both' ] );
};